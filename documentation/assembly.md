Plotbot assembly version 2
=================
This document describes how to assemble Plotbot (version 2).

Plotbot itself is a small currently Arduino based roboter to teach young people
how to code. I went great length to create a robot which is low cost and
precise. Death reckoning is much more precise than other toy robots. Computation
is done in 1µm (wheel diameter is even given in 0.1µm).

Currently there is no bill of material so you need to hope to have all parts
available. Hopefully i will find some time in the future to create
aforementioned bill. 

But without much ado lets start assembling the robot.

Axle assembly
-------------
Dificulty is high for the next step. Be calm and don't hurt yourself!

If the axle is already on its place like in the second image you can jump to the
next section!

![Mounting of the gear wheel axle.](images/1.jpg)

Be aware to grab the axle very tight above the plasic survace to don't do any
harm if you slip. Push in one milimeter and the regrab the axle another
milimeter. Take your time the opening is to small for the axle to ensure
a tight fit. Turn the axle around its center to drill away the plastic which is
in its way.

![Use some pliers to firmly push the axle through the opening.](images/5.jpg)

Gear wheel assembly
-------------------
Difficulty for this step is low with vice, moderate without.

If the ball bearing is already in its place you are lucky and can skip this
step.

![Ball bearing and main pen gear](images/6.jpg)

Put the ball bearing above the opening and try to keep it level with the gear.
Firmly press the bearing into the opening visible in the above image. If you are
using a vice be careful not to damage the gear!

![Ready placed gear](images/7.jpg)

The ball bearing should be nearly level with the gear opening.

Pen motor assembly
------------------
Difficulty moderate (some fiddeling nessecary)

Take the first stepper motor and have the parts (Pen gear and chassis)
available. Place the motor into the chassis as visible in the below image:
 
![Motor placed into chassis.](images/8.jpg) 

Take the gear and place it in line with the axle mounted in the first step.
Take the pliers from first step and firmly press the axle into the ball bearing
of the gear.
 
![Axle mounted inside ball bearing.](images/9.jpg) 

Now try to find the right angle of the gear by slowly turing the gear on the
ball bearing and gently pushing the motor torwards the gear. There should be no
force needed in this step!
In the last step take two M2.5 screws and nuts. Put the nuts into the openings
into the plastic. If you are lucky the should stay fixed if not use your finger
to fixate the nut. 

![Motor and nuts on the right place.](images/10.jpg) 

Be aware that there is an opening on in the chassis for the
lower screw. 

![Pen motor mounted with gear.](images/11.jpg) 

Now take the penholder and gently push it into the opening from below of the
robot. The smaller hole should be visible! You are not able to push the
penholder into the robot with the big hole outside (at least without breaking
something).

![Pen holder put into its place.](images/16.jpg) 

The resistance should be coming mainly from the stepper motor turned via the pen
gear. If there is more resistance you need to sand the penholder and the opeing
until it works without problems.

Mounting of the arduino uno board
---------------------------------
Difficulty easy.

Take your Arduino Uno board and place it on top of the cassis. Take 4 M2.5 with
nuts screes to tighten the board on top of the chassis. Be aware not to
overtighten the screes in order not to damage the Arduino Uno board.

![Arduino Uno board placed on top of the chasssis.](images/17.jpg) 

Mounting of the drive motors
----------------------------
Difficulty easy.

This step might be already done if you are lucky, check if the nuts are already
in place. Otherwise take 4 M2.5 nuts and push them into their place. Be careful to
watch for the orientation of the nuts. Its easy to use some small pliers to
first grab into the nut and sticking them in the first way. After that use the
tip of the pliers to push the nuts in all the way until they are aligned with
the screw openings.

![M2.5 nut on its way.](images/19.jpg) 

Next take the wheels and put the rubber O-rings around them.

![Wheels and 0-rings pre assembly.](images/20.jpg) 

Place to 0-ring onto the wheel and work from both sides until it snaps on the
wheel:

![Wheels assembly.](images/21.jpg) 

Place 4 M2.5 nuts into the wheels and use 4 M2.5 grub screws to tighten the
wheels. If you don't have any grub screws you can also use some normal screws
als long as they are not to long.

![Wheels assembly.](images/22.jpg) 

The axle opening should not be opstructed by the screws.

Place the motor into its opening. Take care that the cable is placed in the
cable channel. Then take the wheel assembly an push it onto the axle of the
motor. After that take two countersunk M2.5 head screws and use them to tighten
the motor clip and the motors together. The whole complete assembly for one side
should look like on the image:

![Drive assembly left.](images/23.jpg) 

Repeat all the steps on the other side. Be careful not do damge the other side.

Take the plug and firmly bend the cable 90°. The plug should go without any
problems through the cable channel. Repeate for the other side.

![Cable on its way through the cable cannel.](images/25.jpg) 

Take the hood and the 8mm steel bal and firmly push the ball into the plastic
part. Be sure to press directly from above the hole not breaking the plastic
part!

![Steel ball and hood.](images/26.jpg) 



![Steel ball and hood assembled.](images/27.jpg) 

Power assembly
--------------
Difficulty easy but be aware of polarisation from power supply!

Take the 3 motor drivers from their antistatic bag and place them into the
cassis. There should be no force needed to place these parts. They might fight
only loosely. Which is fine as the are going to be keept in place by the hood.

![Motor drivers mounted in the cassis.](images/28.jpg) 

Place the motor plugs into the motor drivers. The left motor goes into the
driver farthest away in the below image. The pen driver motor gets plugged into
the middle driver.

![Motor cables are plugged in.](images/29.jpg) 

Be sure to store the motor cables away so that they won't get cought in the pen
gear. 

No use the 4 wire cables female part onto the motor drivers. Use the same color
scheme for both motors to avoid erros if possible. Only the pins marked with
IN1-In4 are used. Keep the other pins free as you can see on the next picture.

![First Arduino connection cables are mounted.](images/30.jpg) 

The left motor goes to pins 1-4 of the arduino. The right motor to pins 5-8. The
pen motor goes to pins 9-12. Keep the lowest pin of the motor driver on the
lowest corresponding pin nr on the Arduino.

![The cables are mounted on the Arduino side.](images/32.jpg) 

Take a breadboard and push it into the openings (or tape it to the plotbot).
Use the power supply cable to supply all driver boards with power. Vcc is +. GND
is Ground. All ground pins should be connected to each other. Also all Vcc pins
should be connected to each other.

Make again sure that you got the power supply lines connected properly to the
power supply lines of the motor drivers! Otherwise you might damage your
hardware! Then take care that all the cables are firmly on the motor drivers.
Take care that none of the cable is in the working range of the pin gear.

![Cable management in the belly of the robot.](images/33.jpg) 

Firmly tighten the screws of the wheels. Now its time for a test run without
hood. Take the version2 firmware of the arduino directory and write the software
to the robot by connecting the usb cable to the arduino board. First the driver
LEDs of the pen driver should light up and the pen motor should move. After that
check that the drive motors start moving. If not recheck the cabling until all
motors move properly.

If all motors are moving then you can place the hood on the chassis.

Assembly is finished. Calibration is the next step you should do.

Congrats for your assembly. I hope you have much fun with your plotbot.

![Wideangle shot of complete robot assembly.](images/34.jpg) 

